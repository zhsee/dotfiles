#! /usr/bin/bash

num_of_cards=$1

suits=(♠️  ❤️  ♣️  ♦️ )
nums=(1 2 3 4 5 6 7 8 9 10 J Q K)

deck=()
for suit in "${suits[@]}"; do
    for num in "${nums[@]}"; do
        deck+=("$num$suit")
    done
done

for _ in $(seq 1 "$num_of_cards"); do
    remaining_cards=${#deck[@]}
    pick=${deck[(( $RANDOM % $remaining_cards + 1))]}
    echo -n " $pick "
    deck=( ${deck[@]/$pick} )
done
echo ""
